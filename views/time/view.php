<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Time */

$this->title = $model->project->title.' / #'.$model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('timetracker', 'Times'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="time-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('timetracker', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('timetracker', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('timetracker', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            [
                'attribute' => 'projectId',
                'format' => 'html',
                'value' => Html::a($model->project->title, ['project/view', 'id'=>$model->projectId]),
            ],
            [
                'attribute' => 'date',
                'value' => Yii::$app->formatter->asDate($model->date, 'medium'),
                ],
            'startTime',
            'endTime',
            'duration',
            'description:ntext',
            'hourrate',
            'cost',
        ],
    ]) ?>

</div>

<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\TimeSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('timetracker', 'Times');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="time-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('timetracker', 'New Time'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            [
                'class' => 'yii\grid\ActionColumn',
                'template'=> '{view} {update}',
                'urlCreator' => function($action, $model, $key, $index) {
                    // using the column name as key, not mapping to 'id' like the standard generator
                    $params = is_array($key) ? $key : [$model->primaryKey()[0] => (string) $key];
                    $params[0] = \Yii::$app->controller->id ? \Yii::$app->controller->id . '/' . $action : $action;
                    return \yii\helpers\Url::toRoute($params);
                },
                'contentOptions' => ['nowrap'=>'nowrap']
            ],
            ['class' => 'yii\grid\SerialColumn'],
            // 'id',
            [
                'attribute' => 'projectTitle',
                'format' => 'html',
                'value' => function ($model, $key, $index, $column) {
                    return Html::a($model->project->title, ['project/view', 'id'=>$model->projectId]);
                },
            ],
            [
                'attribute' => 'date',
                'value' => function ($model, $key, $index, $column) {
                    return Yii::$app->formatter->asDate($model->date, 'medium');
                },
            ],
            'startTime',
            'endTime',
            'duration',
            // 'description:ntext',

            [
                'class' => 'yii\grid\ActionColumn',
                'template'=> '{delete}',
                'urlCreator' => function($action, $model, $key, $index) {
                    // using the column name as key, not mapping to 'id' like the standard generator
                    $params = is_array($key) ? $key : [$model->primaryKey()[0] => (string) $key];
                    $params[0] = \Yii::$app->controller->id ? \Yii::$app->controller->id . '/' . $action : $action;
                    return \yii\helpers\Url::toRoute($params);
                },
                'contentOptions' => ['nowrap'=>'nowrap']
            ],
        ],
    ]); ?>

</div>

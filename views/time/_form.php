<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\MaskedInput;

use kartik\datecontrol\DateControl;
use kartik\widgets\DepDrop;

use app\models\Project;

/* @var $this yii\web\View */
/* @var $model app\models\Time */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="time-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'projectId')->dropDownList(ArrayHelper::map(Project::find()->all(), 'id', 'title'), array('prompt'=>Yii::t('timetracker', '(Select)'))) ?>

    <?= $form->field($model, 'date')->widget(DateControl::classname(), [
        'type'=>DateControl::FORMAT_DATE,
        //'ajaxConversion'=>false,
        'displayFormat' => \yii::$app->formatter->dateFormat,
        'options' => [
            'pluginOptions' => [
                'autoclose' => true
            ]
        ]
    ]); ?>

    <?= $form->field($model, 'startTime')->widget(MaskedInput::classname(), ['mask' => '99:99']) ?>

    <?= $form->field($model, 'endTime')->widget(MaskedInput::classname(), ['mask' => '99:99']) ?>

    <?= $form->field($model, 'duration')->textInput(['maxlength' => 10]) ?>

    <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'hourrate')->textInput(['maxlength' => 10]) ?>
    <?= $form->field($model, 'hourrateSelect')->widget(DepDrop::classname(), [
        'options'=>['id'=>'hourrateSelect-id'],
        'pluginOptions'=>[
            'depends'=>['time-projectid'],
            'placeholder'=>Yii::t('timetracker','(Select)'),
            'url'=>\yii\helpers\Url::to(['/hourrate/subcat'])
        ]
    ]); ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('timetracker', 'Create') : Yii::t('timetracker', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php $this->registerJs(
    '$("document").ready(function(){ $("form select, input:text, form textarea").first().focus(); });'
); ?>

<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Project */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('timetracker', 'Projects'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="project-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('timetracker', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('timetracker', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('timetracker', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            [
                'attribute' => 'userId',
                'value' => $model->user->profile->name,
            ],
            'title',
            'description:ntext',
            [
                'attribute' => Yii::t('timetracker','Times'),
                'format' => 'html',
                'value'=>Html::a(Yii::t('timetracker','Times'),['time/index', 'TimeSearch[projectId]' => $model->id]),
            ],
            [
                'attribute' => Yii::t('timetracker','Hour Rates'),
                'format' => 'html',
                'value'=>Html::a(Yii::t('timetracker','Hour Rates'),['hourrate/index', 'HourrateSearch[projectId]' => $model->id]),
            ],
        ],
    ]) ?>

</div>

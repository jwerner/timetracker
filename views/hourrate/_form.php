<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

use app\models\Project;

/* @var $this yii\web\View */
/* @var $model app\models\Hourrate */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="hourrate-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'projectId')->dropDownList(ArrayHelper::map(Project::find()->all(), 'id', 'title'), array('prompt'=>Yii::t('timetracker', '(Select)'))) ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => 255]) ?>

    <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'rate')->textInput(['maxlength' => 10]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('timetracker', 'Create') : Yii::t('timetracker', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

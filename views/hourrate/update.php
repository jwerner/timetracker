<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Hourrate */

$this->title = Yii::t('timetracker', 'Update {modelClass}: ', [
    'modelClass' => 'Hourrate',
]) . ' ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('timetracker', 'Hourrates'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('timetracker', 'Update');
?>
<div class="hourrate-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

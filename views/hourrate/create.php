<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Hourrate */

$this->title = Yii::t('timetracker', 'Create {modelClass}', [
    'modelClass' => 'Hourrate',
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('timetracker', 'Hourrates'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="hourrate-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

<?php
use kartik\datecontrol\Module;

return [
    // cerulean
    // cosmo
    // cyborg
    // darkly
    // flatly
    // journal
    // lumen
    // paper
    // readable
    // sandstone
    // simplex
    // slate
    // spacelab
    // superhero
    // united
    // yeti
    'theme'=>'spacelab',
    'companyName'=>"Diggin' Data",
    'adminEmail' => 'admin@example.com',
    // Users Module / Admin usernames
    'userAdmins' => ['jwerner'],
    // format settings for saving each date attribute (PHP format example)
    'dateControlSave' => [
        Module::FORMAT_DATE => 'php:Y-m-d', // saves as unix timestamp
        Module::FORMAT_TIME => 'php:H:i:s',
        Module::FORMAT_DATETIME => 'php:Y-m-d H:i:s',
    ]
];

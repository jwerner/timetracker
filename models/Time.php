<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%tt_time}}".
 *
 * @property integer $id
 * @property integer $projectId
 * @property string $date
 * @property string $startTime
 * @property string $endTime
 * @property string $duration
 * @property string $description
 * @property string $hourrate
 *
 * @property TtProject $project
 */
class Time extends \yii\db\ActiveRecord
{
    public $hourrateSelect;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%tt_time}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['projectId', 'date', 'startTime', 'endTime', 'duration'], 'required'],
            [['projectId'], 'integer'],
            [['date', 'startTime', 'endTime'], 'safe'],
            [['duration'], 'number'],
            [['description'], 'string'],
            [['hourrateSelect'], 'integer'],
            [['hourrate'], 'number']
        ];
    }

    public function beforeValidate()
    {
        if (parent::beforeValidate()) {
            if(empty($this->hourrate) and !empty($this->hourrateSelect)) {
                $hourrate = Hourrate::findOne($this->hourrateSelect);
                $this->hourrate = $hourrate->rate;
            }
            return true;
        }
        return false;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('timetracker', 'ID (Prim. Key)'),
            'projectId' => Yii::t('timetracker', 'Project (FK)'),
            'projectTitle' => Yii::t('timetracker', 'Project'),
            'date' => Yii::t('timetracker', 'Date'),
            'startTime' => Yii::t('timetracker', 'Start Time'),
            'endTime' => Yii::t('timetracker', 'End Time'),
            'duration' => Yii::t('timetracker', 'Duration'),
            'description' => Yii::t('timetracker', 'Description'),
            'hourrate' => Yii::t('timetracker', 'Hour Rate'),
            'hourrateSelect' => Yii::t('timetracker', 'Hour Rate'),
            'cost' => Yii::t('timetracker', 'Cost'),
        ];
    }

    public function extraFields()
    {
        return ['project'];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProject()
    {
        return $this->hasOne(Project::className(), ['id' => 'projectId']);
    }

    /* Getter for project title */
    public function getProjectTitle() {
        return $this->project->title;
    }

    public function getCost()
    {
        return $this->duration * $this->hourrate;
    }
}

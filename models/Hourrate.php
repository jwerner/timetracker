<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%tt_hourrate}}".
 *
 * @property integer $id
 * @property integer $projectId
 * @property string $title
 * @property string $description
 * @property string $rate
 *
 * @property TtProject $project
 */
class Hourrate extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%tt_hourrate}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['projectId', 'title'], 'required'],
            [['projectId'], 'integer'],
            [['description'], 'string'],
            [['rate'], 'number'],
            [['title'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('timetracker', 'ID'),
            'projectId' => Yii::t('timetracker', 'Project (FK)'),
            'title' => Yii::t('timetracker', 'Title'),
            'projectTitle' => Yii::t('timetracker', 'Project'),
            'description' => Yii::t('timetracker', 'Description'),
            'rate' => Yii::t('timetracker', 'Hour Rate'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProject()
    {
        return $this->hasOne(Project::className(), ['id' => 'projectId']);
    }

    /* Getter for project title */
    public function getProjectTitle() {
        return $this->project->title;
    }
}

<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Time;

/**
 * TimeSearch represents the model behind the search form about `app\models\Time`.
 */
class TimeSearch extends Time
{
    /* calculated attribute */
    public $projectTitle;
    
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'projectId'], 'integer'],
            [['projectTitle', 'date', 'startTime', 'endTime', 'description'], 'safe'],
            [['duration'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     *
     * @see http://www.yiiframework.com/wiki/621/filter-sort-by-calculated-related-fields-in-gridview-yii-2-0/
     */
    public function search($params)
    {
        $query = Time::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        /**
         * Setup your sorting attributes
         * Note: This is setup before the $this->load($params) 
         * statement below
         */
         $dataProvider->setSort([
            'attributes' => [
                'id',
                'projectTitle' => [
                    'asc' => ['tt_project.title' => SORT_ASC],
                    'desc' => ['tt_project.title' => SORT_DESC],
                    'label' => 'Project'
                ],
                'date',
                'startTime',
                'endTime',
                'duration'
            ]
        ]);


        /*
        $this->load($params);
        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
         */
        if (!($this->load($params) && $this->validate())) {
            /**
             * The following line will allow eager loading with country data 
             * to enable sorting by country on initial loading of the grid.
             */ 
            $query->joinWith(['project']);
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'projectId' => $this->projectId,
            'date' => $this->date,
            'startTime' => $this->startTime,
            'endTime' => $this->endTime,
            'duration' => $this->duration,
        ]);

        $query->andFilterWhere(['like', 'description', $this->description]);

        // filter by project title
        $query->joinWith(['project' => function ($q) {
            $q->where('tt_project.title LIKE "%' . $this->projectTitle . '%"');
        }]);

        return $dataProvider;
    }
}

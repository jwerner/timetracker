<?php

namespace app\models;

use Yii;
use yii\helpers\Url;
use yii\web\Link;
use yii\web\Linkable;

use \dektrium\user\models\User;

/**
 * This is the model class for table "{{%tt_project}}".
 *
 * @property integer $id
 * @property integer $userId
 * @property string $title
 * @property string $description
 *
 * @property User $user
 * @property TtTime[] $ttTimes
 */
class Project extends \yii\db\ActiveRecord implements \yii\web\Linkable
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%tt_project}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['userId', 'title'], 'required'],
            [['userId'], 'integer'],
            [['description'], 'string'],
            [['title'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('timetracker', 'ID'),
            'userId' => Yii::t('timetracker', 'User ID (FK)'),
            'username' => Yii::t('timetracker', 'User Name'),
            'title' => Yii::t('timetracker', 'Project Title'),
            'description' => Yii::t('timetracker', 'Description'),
        ];
    }
    
    public function getLinks()
    {
        return [
            Link::REL_SELF => Url::to(['../web?r=project/view&id=' . $this->id], true),
        ];
    }

    public function extraFields()
    {
        return ['times'];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'userId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTimes()
    {
        return $this->hasMany(Time::className(), ['projectId' => 'id'])->orderBy('date');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getHourrates()
    {
        return $this->hasMany(Hourrate::className(), ['projectId' => 'id']);
    }
}

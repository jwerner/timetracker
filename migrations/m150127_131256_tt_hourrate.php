<?php

use yii\db\Schema;
use yii\db\Migration;

class m150127_131256_tt_hourrate extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('{{%tt_hourrate}}', [
            'id'            => Schema::TYPE_PK,

            'projectId'     => Schema::TYPE_INTEGER . "(11) NOT NULL COMMENT 'Project ID (FK)'",
            'title'         => Schema::TYPE_STRING . "(255) COLLATE utf8_unicode_ci NOT NULL COMMENT 'Project Title'",
            'description'   => Schema::TYPE_TEXT . " COLLATE utf8_unicode_ci COMMENT 'Description'",            
            'rate'          => Schema::TYPE_DECIMAL . "(10,2) DEFAULT 0 COMMENT 'Hour Rate'",            
        ], $tableOptions);
        $this->createIndex('projectId', '{{%tt_hourrate}}', 'projectId', false);
        $this->addForeignKey('fk_project_hourrate', '{{%tt_hourrate}}', 'projectId', '{{%tt_project}}', 'id', 'CASCADE', 'CASCADE');

    }

    public function down()
    {
        $this->dropTable('{{%tt_hourrate}}');
    }
}

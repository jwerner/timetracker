<?php

use yii\db\Schema;
use yii\db\Migration;

class m150122_093143_tt_project extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('{{%tt_project}}', [
            'id'            => Schema::TYPE_PK,
            'userId'        => Schema::TYPE_INTEGER . "(11) NOT NULL COMMENT 'User ID (FK)'",
            'title'         => Schema::TYPE_STRING . "(255) COLLATE utf8_unicode_ci NOT NULL COMMENT 'Project Title'",
            'description'   => Schema::TYPE_TEXT . " COLLATE utf8_unicode_ci COMMENT 'Description'",            
        ], $tableOptions);
        $this->createIndex('userId', '{{%tt_project}}', 'userId', false);
        $this->addForeignKey('fk_user_project', '{{%tt_project}}', 'userId', '{{%user}}', 'id', 'CASCADE', 'RESTRICT');

    }

    public function down()
    {
        $this->dropTable('{{%tt_project}}');
    }
}

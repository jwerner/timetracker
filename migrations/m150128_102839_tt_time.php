<?php

use yii\db\Schema;
use yii\db\Migration;

class m150128_102839_tt_time extends Migration
{
    public function up()
    {
        $this->addColumn('{{%tt_time}}', 'hourrate', Schema::TYPE_DECIMAL . "(10,2) NULL COMMENT 'Hour Rate'");
    }

    public function down()
    {
        $this->dropColumn('{{%tt_time}}', 'hourrate');
    }
}

<?php

use yii\db\Schema;
use yii\db\Migration;

class m150122_111924_tt_time extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('{{%tt_time}}', [
            'id'            => Schema::TYPE_PK . " COMMENT 'ID (Prim. Key)'",
            'projectId'        => Schema::TYPE_INTEGER . "(11) NOT NULL COMMENT 'Project (FK)'",
            'date'          => Schema::TYPE_DATE . " NOT NULL COMMENT 'Date'",
            'startTime'     => Schema::TYPE_TIME . " NOT NULL COMMENT 'Start Time'",
            'endTime'    => Schema::TYPE_TIME . " NOT NULL COMMENT 'End Time'",
            'duration'      => Schema::TYPE_DECIMAL . "(10,2) NOT NULL COMMENT 'Duration'",
            'description'   => Schema::TYPE_TEXT . " NULL COMMENT 'Description'",
            ],
            $tableOptions
        );
        $this->createIndex('projectId', '{{%tt_time}}', 'projectId', false);
        $this->addForeignKey('fk_project_time', '{{%tt_time}}', 'projectId', '{{%tt_project}}', 'id', 'CASCADE', 'RESTRICT');
    }

    public function down()
    {
        $this->dropTable('{{%tt_time}}');
    }
}

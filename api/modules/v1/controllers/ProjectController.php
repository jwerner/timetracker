<?php
namespace app\api\modules\v1\controllers;

use yii\rest\ActiveController;

class ProjectController extends ActiveController
{
    public $modelClass = 'app\models\Project';
}

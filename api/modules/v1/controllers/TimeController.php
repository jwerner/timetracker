<?php
namespace app\api\modules\v1\controllers;

use yii\rest\ActiveController;

class TimeController extends ActiveController
{
    public $modelClass = 'app\models\Time';
}


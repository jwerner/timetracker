<?php

return [
    // cerulean
    // cosmo
    // cyborg
    // darkly
    // flatly
    // journal
    // lumen
    // paper
    // readable
    // sandstone
    // simplex
    // slate
    // spacelab
    // superhero
    // united
    // yeti
    'theme'=>'spacelab',
    'companyName'=>"Diggin' Data",
    'adminEmail' => 'admin@example.com',
    // Users Module / Admin usernames
    'userAdmins' => ['jwerner'],
    // format settings for saving each date attribute (PHP format example)
];
